Будут проигнорированы:
1. Все файлы в каталогах .terraform в любом из каталогов terraform  
2. Все файлы заканчивающиеся на .tfstate
3. Все файлы содержащие в названии .tfstate.
4. Файлы crash.log
5. Все файлы заканчивающиеся на .tfvars
6. Файлы override.tf
7. Файлы override.tf.json
8. Все файлы заканчивающиеся на _override.tf
9. Все файлы заканчивающиеся на _override.tf.json
11. Файлы .terraformrc
12. Файлы terraform.rc